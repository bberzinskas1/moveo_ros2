// pre-microros sketch that does mqtt to steppers. . this helped me get the motor
// config right and gives a basic test bed.

#include <ArduinoJson.h>
#include <EspMQTTClient.h>
#include <ESP_FlexyStepper.h>
#include <Servo.h>

EspMQTTClient client(
  "ATTAcUw6VS",
  "47044704",
  "192.168.0.230",  // MQTT Broker server ip
  "",   // Can be omitted if not needed
  "",   // Can be omitted if not needed
  "moveo",     // Client name that uniquely identify your device
  1883              // The MQTT port, default to 1883. this line can be omitted
);

// create the stepper motor object
ESP_FlexyStepper stepper1;
ESP_FlexyStepper stepper2;
ESP_FlexyStepper stepper3;
ESP_FlexyStepper stepper4;
ESP_FlexyStepper stepper5;
ESP_FlexyStepper stepper6;

static const int servoPin = 18;
Servo gripper;

void setupSteppers(){



  // hand rotate
  stepper1.connectToPins(16,17);

  //wrist tilt
  stepper2.connectToPins(4,5);

  // wrist twist
  stepper3.connectToPins(12, 13);

  // elbow tilt
  stepper4.connectToPins(14, 15);

  // shoulder tilt  
  stepper5.connectToPins(19, 21);

  //shoulder rotate
  stepper6.connectToPins(23, 25);

  stepper1.setSpeedInStepsPerSecond(50);
  stepper1.setAccelerationInStepsPerSecondPerSecond(100);
  stepper1.setDecelerationInStepsPerSecondPerSecond(100);

  stepper2.setSpeedInStepsPerSecond(50);
  stepper2.setAccelerationInStepsPerSecondPerSecond(100);
  stepper2.setDecelerationInStepsPerSecondPerSecond(100);

  stepper3.setSpeedInStepsPerSecond(50);
  stepper3.setAccelerationInStepsPerSecondPerSecond(100);
  stepper3.setDecelerationInStepsPerSecondPerSecond(100);

  stepper4.setSpeedInStepsPerSecond(50);
  stepper4.setAccelerationInStepsPerSecondPerSecond(100);
  stepper4.setDecelerationInStepsPerSecondPerSecond(100);

  stepper5.setSpeedInStepsPerSecond(50);
  stepper5.setAccelerationInStepsPerSecondPerSecond(100);
  stepper5.setDecelerationInStepsPerSecondPerSecond(100);

  stepper6.setSpeedInStepsPerSecond(50);
  stepper6.setAccelerationInStepsPerSecondPerSecond(100);
  stepper6.setDecelerationInStepsPerSecondPerSecond(100);

  stepper1.startAsService();
  stepper2.startAsService();
  stepper3.startAsService();
  stepper4.startAsService();
  stepper5.startAsService();
  stepper6.startAsService();

  gripper.attach(
    servoPin,
    Servo::CHANNEL_NOT_ATTACHED,
    45,
    115
  );

}


void setup(){
  Serial.begin(115200);
  DynamicJsonDocument doc(200);

  // Optionnal functionnalities of EspMQTTClient :
  client.enableDebuggingMessages(); // Enable debugging messages sent to serial output
  //client.enableHTTPWebUpdater(); // Enable the web updater. User and password default to values of MQTTUsername and MQTTPassword. These can be overrited with enableHTTPWebUpdater("user", "password").
  //client.enableLastWillMessage("moveo/lastwill", "moveo going offline!");  // You can activate the retain flag by setting the third parameter to true

  setupSteppers();
}

void handleSteppers(String payload){
  // stepper message is a json array, deserialize / type each one
  StaticJsonDocument<256> doc;
  deserializeJson(doc, payload);

  int32_t axisSteps_1 = doc[0];
  stepper1.setTargetPositionRelativeInSteps(axisSteps_1);

  int32_t axisSteps_2 = doc[1];
  stepper2.setTargetPositionRelativeInSteps(axisSteps_2);

  int32_t axisSteps_3 = doc[2];
  stepper3.setTargetPositionRelativeInSteps(axisSteps_3);

  int32_t axisSteps_4 = doc[3];
  stepper4.setTargetPositionRelativeInSteps(axisSteps_4);

  int32_t axisSteps_5 = doc[4];
  stepper5.setTargetPositionRelativeInSteps(axisSteps_5);

  int32_t axisSteps_6 = doc[5];
  stepper6.setTargetPositionRelativeInSteps(axisSteps_6);
}

void handleServo(String payload){
  Serial.println(payload);
  StaticJsonDocument<256> doc;
  deserializeJson(doc, payload);
  int gripper_pos = doc[0];
  gripper.write(gripper_pos);
}

// This function is called once everything is connected (Wifi and MQTT)
void onConnectionEstablished(){

  // Subscribe to "moveo/stepper" and  update the robot pose
  client.subscribe("moveo/stepper", [](const String & payload) {
    Serial.println(payload);
    handleSteppers(payload);
  });

    // Subscribe to "moveo/gripper" and update the servo
  client.subscribe("moveo/gripper", [](const String & payload) {
    handleServo(payload);
  });
}


void loop(){
  client.loop();
}
