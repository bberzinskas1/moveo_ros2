#include <stdbool.h>
#include "freertos/FreeRTOS.h"
#include <freertos/task.h>
#include <esp_log.h>
#include <driver/gpio.h>
#include "motors.h"
#include "utils.h"
#include "acceleration.h"

#define STEPS_PER_REVOLUTION 20
#define STEP_RESOLUTION 16

#define uS_IN_MINUTE 60000000

typedef struct Motor {
    int step;
    int dir;
    int enabled;
} Motor;

uint32_t feed_to_delay(int feed);

int leading_axis_steps(int new_x_pos, int new_y_pos);

void doSteps(int x0, int y0, int z0, int a0, int b0, int c0, int x1, int y1, int z1, int a1, int b1, int c1, uint32_t motor_delay);

Motor motor_x = {
        .step = 23,
        .dir = 25,
};
Motor motor_y = {
        .step = 19,
        .dir = 21,
};
Motor motor_z = {
        .step = 14,
        .dir = 15,
};
Motor motor_a = {
        .step = 12,
        .dir = 13,
};
Motor motor_b = {
        .step = 4,
        .dir = 5,
};
Motor motor_c = {
        .step = 16,
        .dir = 17,
};

int x_pos = 0;
int y_pos = 0;
int z_pos = 0;
int a_pos = 0;
int b_pos = 0;
int c_pos = 0;

bool motors_enabled;

static const char *TAG = "MOTORS";

void motor_setup(void) {
    gpio_config_t io_conf;
    ESP_LOGI(TAG, "Begin Setup");
    uint64_t motor_x_pins = ( (1ULL << motor_x.step) | (1ULL << motor_x.dir));
    uint64_t motor_y_pins = ( (1ULL << motor_y.step) | (1ULL << motor_y.dir));
    uint64_t motor_z_pins = ( (1ULL << motor_z.step) | (1ULL << motor_z.dir));
    uint64_t motor_a_pins = ( (1ULL << motor_a.step) | (1ULL << motor_a.dir));
    uint64_t motor_b_pins = ( (1ULL << motor_b.step) | (1ULL << motor_b.dir));
    uint64_t motor_c_pins = ( (1ULL << motor_c.step) | (1ULL << motor_c.dir));
    ESP_LOGI(TAG, "Init gpio");
    io_conf.mode = GPIO_MODE_OUTPUT;
    io_conf.pin_bit_mask = (motor_x_pins | motor_y_pins | motor_z_pins | motor_a_pins | motor_b_pins | motor_c_pins);
    io_conf.intr_type = 0;
    io_conf.pull_down_en = 0;
    io_conf.pull_up_en = 0;

    gpio_config(&io_conf);
    ESP_LOGI(TAG, "GPIO configured");

    // gpio_set_level(motor_x.enabled, HIGH);   //turn off power to motors by default
    // gpio_set_level(motor_y.enabled, HIGH);

    motors_enabled = 0;
}

void move(int new_x_pos, int new_y_pos, int new_z_pos, int new_a_pos, int new_b_pos, int new_c_pos, int feed) {
    vTaskSuspendAll();  // TODO try suspending interrupts also?
    //ESP_LOGI(TAG, "Entered Move Steppers");
    if(new_x_pos == -1) {
        new_x_pos = x_pos;
    }
    if(new_y_pos == -1) {
        new_y_pos = y_pos;
    }
    if(new_z_pos == -1) {
        new_z_pos = z_pos;
    }
    if(new_a_pos == -1) {
        new_a_pos = a_pos;
    }
    if(new_b_pos == -1) {
        new_b_pos = b_pos;
    }
    if(new_c_pos == -1) {
        new_c_pos = c_pos;
    }

    new_x_pos = CONSTRAIN(new_x_pos, 0, MAX_STEPS);
    new_y_pos = CONSTRAIN(new_y_pos, 0, MAX_STEPS);
    new_z_pos = CONSTRAIN(new_z_pos, 0, MAX_STEPS);
    new_a_pos = CONSTRAIN(new_a_pos, 0, MAX_STEPS);
    new_b_pos = CONSTRAIN(new_b_pos, 0, MAX_STEPS);
    new_c_pos = CONSTRAIN(new_c_pos, 0, MAX_STEPS);

    uint32_t motor_delay = feed_to_delay(feed);
	//ESP_LOGI(TAG, "Set direction");
    //set motor direction
    if (x_pos > new_x_pos) {
        gpio_set_level(motor_x.dir, LOW);
    } else {
        gpio_set_level(motor_x.dir, HIGH);
    }
    if (y_pos > new_y_pos) {
        gpio_set_level(motor_y.dir, HIGH);
    } else {
        gpio_set_level(motor_y.dir, LOW);
    }
    if (z_pos > new_z_pos) {
        gpio_set_level(motor_z.dir, HIGH);
    } else {
        gpio_set_level(motor_z.dir, LOW);
    }
    if (a_pos > new_a_pos) {
        gpio_set_level(motor_a.dir, HIGH);
    } else {
        gpio_set_level(motor_a.dir, LOW);
    }
    if (b_pos > new_b_pos) {
        gpio_set_level(motor_b.dir, HIGH);
    } else {
        gpio_set_level(motor_b.dir, LOW);
    }
    if (c_pos > new_c_pos) {
        gpio_set_level(motor_c.dir, HIGH);
    } else {
        gpio_set_level(motor_c.dir, LOW);
    }
    doSteps(x_pos, y_pos, z_pos, a_pos, b_pos, c_pos, new_x_pos, new_y_pos, new_z_pos, new_a_pos, new_b_pos, new_c_pos, 20);
    xTaskResumeAll();

    x_pos = new_x_pos;
    y_pos = new_y_pos;
    z_pos = new_z_pos;
    a_pos = new_a_pos;
    b_pos = new_b_pos;
    c_pos = new_c_pos;

    ESP_LOGI(TAG, "New pos - %d, %d, %d, %d, %d, %d", x_pos, y_pos, z_pos, a_pos, b_pos, c_pos);
}

void doSteps(int x0, int y0, int z0, int a0, int b0, int c0, int x1, int y1, int z1, int a1, int b1, int c1, uint32_t motor_delay) {
    int dx = x1 - x0;
    int dy = y1 - y0;
    int dz = z1 - z0;
    int da = a1 - a0;
    int db = b1 - b0;
    int dc = c1 - c0;

    int xi = 1;
    if (dx < 0) {
        xi = -1;
        dx = -dx;
    }
    int d = 2 * dx - dy;
    int x = x0;

    for (int y = y0; y < y1; y++) {
        gpio_set_level(motor_y.step, HIGH);
        if (d > 0) {
            gpio_set_level(motor_x.step, HIGH);
            x += xi;
            d -= 2 * dy;
        }
        ets_delay_us(5);
        gpio_set_level(motor_x.step, LOW);
        gpio_set_level(motor_y.step, LOW);
        ets_delay_us(motor_delay - 5);
        d += 2 * dx;
    }
}

void motor_power(bool on) {
    gpio_set_level(motor_x.enabled, (uint32_t) !on); //ENABLE pin needs to be low to turn off power
    gpio_set_level(motor_y.enabled, (uint32_t) !on);
    gpio_set_level(motor_z.enabled, (uint32_t) !on);
    gpio_set_level(motor_a.enabled, (uint32_t) !on);
    gpio_set_level(motor_b.enabled, (uint32_t) !on);
    gpio_set_level(motor_c.enabled, (uint32_t) !on);
    motors_enabled = on;
    ets_delay_us(5);
    ESP_LOGI(TAG, "Motor power switched %s", on ? "ON" : "OFF");
}


uint32_t feed_to_delay(int feed) {
    uint32_t motorDelay;
    feed = CONSTRAIN(feed, feed, MAX_FEED);
    if (feed <= 0) {
        feed = NORMAL_FEED;
    }
    motorDelay = (uint32_t) (uS_IN_MINUTE / (feed * STEPS_PER_MM));
    return motorDelay;
}

int leading_axis_steps(int new_x_pos, int new_y_pos) {
    int delta_x = abs(new_x_pos - x_pos);
    int delta_y = abs(new_y_pos - y_pos);
    return MAX(delta_x, delta_y);
}
