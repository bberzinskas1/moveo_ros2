#ifndef MOVEO_MAIN_H
#define MOVEO_MAIN_H

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include <freertos/queue.h>

#define QUEUE_SIZE 20

extern QueueHandle_t queue;

void process_commands(void *pvParameters);
void app_main(void);
// void restart(void);

#endif
