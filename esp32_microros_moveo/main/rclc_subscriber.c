#include <rcl/rcl.h>
#include <rcl/error_handling.h>
#include <rclc/rclc.h>
#include <rclc/executor.h>
#include <rmw_uros/options.h>
#include <std_msgs/msg/int16.h>
#include "sensor_msgs/msg/joint_state.hpp"

#include "esp_log.h"
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "servo.h"
#include "motors.h"
static const char *TAG = "ros";
#define RCCHECK(fn) { rcl_ret_t temp_rc = fn; if((temp_rc != RCL_RET_OK)){printf("Failed status on line %d: %d. Aborting.\n",__LINE__,(int)temp_rc); return 1;}}
#define RCSOFTCHECK(fn) { rcl_ret_t temp_rc = fn; if((temp_rc != RCL_RET_OK)){printf("Failed status on line %d: %d. Continuing.\n",__LINE__,(int)temp_rc);}}

rcl_subscription_t gripperSubscriber;
rcl_subscription_t stepperSubscriber;
std_msgs__msg__Int16 gripperMsg;
control_msgs__msg__JointTrajectoryControllerState stepperMsg;

void gripper_callback(const void * msgin)
{
	ESP_LOGI(TAG, "GripperCallback");
	const std_msgs__msg__Int16 * gripperMsg = (const std_msgs__msg__Int16 *)msgin;
	int payload = gripperMsg->data;
	printf("I have heard: \"%d\"\n", payload);
	move_servo((int)payload);

	// todo: add message to queue
//  Command command = parse_buffer(line_buffer, line_len);
//		xQueueSend(queue, &command, portMAX_DELAY);

}

void stepper_callback(const void * msgin)
{
		ESP_LOGI(TAG, "StepperCallback");
	const control_msgs__msg__JointTrajectoryControllerState * stepperMsg = (const control_msgs__msg__JointTrajectoryControllerState *)msgin;
	int payload = stepperMsg->data;
	printf("I have heard: \"%d\"\n", payload);
	move((int)payload,50,50,50,50,50,30);

	// todo: add message to queue
//  Command command = parse_buffer(line_buffer, line_len);
//		xQueueSend(queue, &command, portMAX_DELAY);

}

int rosAppMain(int argc, const char * const * argv)
{
	rcl_allocator_t allocator = rcl_get_default_allocator();
	rclc_support_t support;
	rcl_init_options_t init_options = rcl_get_zero_initialized_init_options();
	RCCHECK(rcl_init_options_init(&init_options, allocator));
	rmw_init_options_t* rmw_options = rcl_init_options_get_rmw_init_options(&init_options);

	// Static Agent IP and port can be used instead of autodisvery.
	RCCHECK(rmw_uros_options_set_udp_address(CONFIG_MICRO_ROS_AGENT_IP, CONFIG_MICRO_ROS_AGENT_PORT, rmw_options));

	// create init_options
	RCCHECK(rclc_support_init_with_options(&support, 0, NULL, &init_options, &allocator));

	// create node
	rcl_node_t node;
	RCCHECK(rclc_node_init_default(&node, "esp32_moveo", "", &support));

	// create subscribers
	ESP_LOGI(TAG, "create gripper subscriber");
	RCCHECK(rclc_subscription_init_default(
		&gripperSubscriber,
		&node,
		ROSIDL_GET_MSG_TYPE_SUPPORT(std_msgs, msg, Int16),
		"gripper"));

  ESP_LOGI(TAG, "create stepper subscriber");

	RCCHECK(rclc_subscription_init_default(
		&stepperSubscriber,
		&node,
		ROSIDL_GET_MSG_TYPE_SUPPORT(control_msgs, msg, JointTrajectoryControllerState),
		"state"));

	// create executor
	ESP_LOGI(TAG, "create executor");
	rclc_executor_t executor = rclc_executor_get_zero_initialized_executor();
	RCCHECK(rclc_executor_init(&executor, &support.context, 2, &allocator));

	RCCHECK(rclc_executor_add_subscription(&executor, &gripperSubscriber, &gripperMsg, &gripper_callback, ON_NEW_DATA));
	RCCHECK(rclc_executor_add_subscription(&executor, &stepperSubscriber, &stepperMsg, &stepper_callback, ON_NEW_DATA));
	ESP_LOGI(TAG, "SPIN!");

	while(true){
			rclc_executor_spin(&executor);
			//usleep(200);
	}

	ESP_LOGI(TAG, "shutdown task");
	RCCHECK(rcl_subscription_fini(&gripperSubscriber, &node));
	RCCHECK(rcl_subscription_fini(&stepperSubscriber, &node));
	RCCHECK(rcl_node_fini(&node));

	vTaskDelete(NULL);
  	return 0;
}
