
#ifndef MOVEO_QUEUE_H
#define MOVEO_QUEUE_H

typedef struct Field {
    char letter;
    double num; // TODO refactor to use float instead of double (find out if need to round in software)
} Field;

typedef struct Command {
    int size;
    Field *fields;
} Command;

void process_commands(void *pvParameters);
void print_command(Command command);

#endif
