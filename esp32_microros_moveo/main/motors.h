#ifndef MOVEO_MOTORS_H
#define MOVEO_MOTORS_H

#include "stdbool.h"

#define HIGH 1
#define LOW 0

#define MAX_STEPS 4320
#define MAX_RANGE_MM 40
#define STEPS_PER_MM ((float)MAX_STEPS/MAX_RANGE_MM)

extern bool motors_enabled;
extern int x_pos;
extern int y_pos;
extern int z_pos;
extern int a_pos;
extern int b_pos;
extern int c_pos;

void motor_setup(void);

void motor_power(bool on);

void move(int new_x_pos, int new_y_pos, int new_z_pos, int new_a_pos, int new_b_pos, int new_c_pos, int feed);

#endif
