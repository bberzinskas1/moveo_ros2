
#include <esp_log.h>
#include <string.h>
#include <ctype.h>

#include "queue.h"
#include "math.h"
#include "servo.h"
#include "main.h"
#include "motors.h"
#include "acceleration.h"
#include "utils.h"

#define COMMAND_LENGTH 20

void process_commands(void *pvParameters) {
    for (;;) {
     Command command;
     xQueueReceive(queue, &command, portMAX_DELAY);
     print_command(command);
   }
}

void print_command(Command command) {
    // TODO only if log level info
    for (int i = 0; i < command.size; i++) {
        printf("%c%.4lf ", command.fields[i].letter, command.fields[i].num);
    }
    printf("\n");
}
