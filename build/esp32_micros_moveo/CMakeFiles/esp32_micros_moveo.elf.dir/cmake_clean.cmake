file(REMOVE_RECURSE
  "bootloader/bootloader.bin"
  "bootloader/bootloader.elf"
  "bootloader/bootloader.map"
  "config/sdkconfig.cmake"
  "config/sdkconfig.h"
  "esp32_micros_moveo.bin"
  "esp32_micros_moveo.map"
  "project_elf_src.c"
  "CMakeFiles/esp32_micros_moveo.elf.dir/project_elf_src.c.obj"
  "esp32_micros_moveo.elf"
  "esp32_micros_moveo.elf.pdb"
  "project_elf_src.c"
)

# Per-language clean rules from dependency scanning.
foreach(lang C)
  include(CMakeFiles/esp32_micros_moveo.elf.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
